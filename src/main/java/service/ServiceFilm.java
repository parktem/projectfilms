package service;

import java.util.List;

import model.Film;
import repository.RepositoryFilm;

public class ServiceFilm {

	RepositoryFilm repository = new RepositoryFilm();

	public void insertFilm(Film film) {
		repository.insertPelicula(film);
	}

	public List<Film> selectAllFilms() {
		return repository.selectAllFilms();
	}

}