package repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import connection.ConnectionH2;
import connection.ConnectionManager;
import model.Film;

public class RepositoryFilm {

	private static final String jdbcUrl = "jdbc:h2:file:./src/main/resources/test";
	ConnectionManager manager = new ConnectionH2();
	
	public void insertPelicula(Film film) {
		Connection conn = manager.open(jdbcUrl);
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = conn.prepareStatement("INSERT INTO PELICULAS (titulo,descripcion, año) VALUES (?, ?, ?)");
			preparedStatement.setString(1, film.getTitle());
			preparedStatement.setString(2, film.getDescription());
			preparedStatement.setInt(3, film.getYear());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}finally {
			close(preparedStatement);
		}
		
		
		manager.close(conn);
	}
	
	private void close(PreparedStatement prepareStatement) {
		try {
			prepareStatement.close();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	private void close(ResultSet resultSet) {
		try {
			resultSet.close();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	public List<Film> selectAllFilms() {
		List<Film> listFilm = new ArrayList<Film>();
		Connection conn = manager.open(jdbcUrl);
		ResultSet resultSet = null;
		PreparedStatement prepareStatement = null;
		try {
			
			prepareStatement = conn.prepareStatement("SELECT * FROM PELICULAS");
			resultSet = prepareStatement.executeQuery();
			while (resultSet.next()) {
				Film filmFromDataBase = new Film();
				filmFromDataBase.setCodFilm(resultSet.getInt(1));
				filmFromDataBase.setTitle(resultSet.getString(2));
				filmFromDataBase.setDescription(resultSet.getString(3));
				filmFromDataBase.setYear(resultSet.getInt(4));
				
				listFilm.add(filmFromDataBase);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} finally {
			if(resultSet != null)
				close(resultSet);
			close(prepareStatement);
			manager.close(conn);
		}

		return listFilm;
	}
	
	
	
}
