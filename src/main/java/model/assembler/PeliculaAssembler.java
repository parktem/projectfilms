package model.assembler;


import javax.servlet.http.HttpServletRequest;

import model.Film;

public class PeliculaAssembler {

    public static Film assemblePeliculaFrom(HttpServletRequest req) {
    	Film film = new Film();
        String codPelicula = req.getParameter("codPelicula");
        if (null != codPelicula) {
            film.setCodFilm((Integer.parseInt(codPelicula)));
        }
        String titulo = req.getParameter("titulo");
        String descripcion = req.getParameter("descripcion");
        String año = req.getParameter("año");
        if (null != año) {
            film.setYear((Integer.parseInt(año)));
        }
        film.setTitle(titulo);
        film.setDescription(descripcion);;
        return film;
    }

}