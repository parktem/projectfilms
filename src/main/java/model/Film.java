package model;

public class Film{

    private int codFilm;
    private String title;
    private String description;
    private int year;
    
	public int getCodFilm() {
		return codFilm;
	}
	public void setCodFilm(int codFilm) {
		this.codFilm = codFilm;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}

    
    
}