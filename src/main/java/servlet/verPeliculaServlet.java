package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Film;
import service.ServiceFilm;

public class verPeliculaServlet extends HttpServlet{
	
	
	private ServiceFilm service = new ServiceFilm();

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		List<Film> listAllFilms = service.selectAllFilms();
		req.setAttribute("listAllFilms", listAllFilms);
		redirect(req,resp);
	}
	
	
	protected void redirect(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/listadoPeliculas.jsp");
		dispatcher.forward(req,resp);
	}
	
}
