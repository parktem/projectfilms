<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ page import="java.io.*,java.util.*,model.*" %>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>List Owners and their pets</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
        crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>	

<script>

	function buscar() {
            var palabra = document.getElementById("buscador").value;
            var table = document.getElementById("table");

            for (let i = 1; i < table.rows.length; i++) {
                for (let j = 0; j < table.rows[i].cells.length; j++) {
                    document.getElementById("table").rows[i].style.display = ""
                }
            }

            for (let i = 1; i < table.rows.length; i++) {
                var coincidencia = false;
                for (let j = 0; j < table.rows[i].cells.length; j++) {
                    if (table.rows[i].cells[j].innerText.toLowerCase().includes(palabra.toLowerCase())) {
                        coincidencia = true;
                    }

                }
                if (coincidencia) {
                    document.getElementById("table").rows[i].style.display = ""
                } else {
                    document.getElementById("table").rows[i].style.display = "none"
                }
            }

     }

</script>

</head>
<body>


<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<div class="col-6 offset-3">
<input onkeyup="buscar()" type="text" class="form-control" id="buscador" placeholder="Buscar pelicula...">
</div>
<br/>
<br/>
<div class="col-12">
<table border="1" id="table" class="table">
	<thead class="thead-dark">
		<tr>
			<th scope="col">Num Pelicula</td>
			<th scope="col">Titulo</td>
			<th scope="col">Descripcion</td>
			<th scope="col">Año</td>
		</tr>
	</thead>
	<tbody>
		<c:forEach var="owner1" items="${listAllPeliculas}">
			<tr>
				<th scope="col"><c:out value="${owner1.codFilm}"/> </th>
				<td><c:out value="${owner1.title}"/> </td>
				<td><c:out value="${owner1.description}"/> </td>
				<td><c:out value="${owner1.year}"/> </td>
	    	</tr>
		</c:forEach>
	</tbody>
</table>
</div>
</body>
</html>