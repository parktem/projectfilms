create table IF NOT EXISTS Peliculas(
	codPelicula INT IDENTITY,
	titulo varchar(50),
	descripcion varchar(200),
	año INT
	PRIMARY KEY (codPelicula)
);
